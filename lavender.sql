-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 07, 2015 at 01:34 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `lavender`
--

-- --------------------------------------------------------

--
-- Table structure for table `account_admin`
--

CREATE TABLE IF NOT EXISTS `account_admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(150) COLLATE utf8_unicode_ci DEFAULT '',
  `remember_token` varchar(150) COLLATE utf8_unicode_ci DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `account_admin_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `account_admin`
--

INSERT INTO `account_admin` (`id`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'm7mdmaree26@gmail.com', '$2y$10$NS2GMFsyMdT25tq9SkT6zefama5EypUyhOV2NqzP2c2CMY/QSosiq', NULL, '2015-05-06 09:06:41', '2015-05-06 09:06:41');

-- --------------------------------------------------------

--
-- Table structure for table `account_customer`
--

CREATE TABLE IF NOT EXISTS `account_customer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_address_id` int(10) unsigned DEFAULT NULL,
  `email` varchar(150) COLLATE utf8_unicode_ci DEFAULT '',
  `password` varchar(150) COLLATE utf8_unicode_ci DEFAULT '',
  `confirmation_code` varchar(150) COLLATE utf8_unicode_ci DEFAULT '',
  `remember_token` varchar(150) COLLATE utf8_unicode_ci DEFAULT '',
  `confirmed` int(11) DEFAULT NULL,
  `store_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `account_customer_customer_address_id_index` (`customer_address_id`),
  KEY `account_customer_store_id_index` (`store_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `account_customer`
--

INSERT INTO `account_customer` (`id`, `customer_address_id`, `email`, `password`, `confirmation_code`, `remember_token`, `confirmed`, `store_id`, `created_at`, `updated_at`) VALUES
(1, NULL, 'm7mdmaree26@gmail.com', '$2y$10$t.Qx28AUpaF3eLft0eakGOjFqIyrjDJCZrEuSqwwHEh.tzEbQfZLu', '3f3ddf7b89625772a305fbf26efdfb47', 'mJXScHStPgMQRlhdClMXOzzGFebHxjxhcNyp6mXt4mDQzcDp0kHLORTxR3Yc', 1, 1, '2015-05-06 09:12:29', '2015-05-06 10:49:39');

-- --------------------------------------------------------

--
-- Table structure for table `account_customer_address`
--

CREATE TABLE IF NOT EXISTS `account_customer_address` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci DEFAULT '',
  `street_1` varchar(150) COLLATE utf8_unicode_ci DEFAULT '',
  `street_2` varchar(150) COLLATE utf8_unicode_ci DEFAULT '',
  `city` varchar(150) COLLATE utf8_unicode_ci DEFAULT '',
  `region` varchar(150) COLLATE utf8_unicode_ci DEFAULT '',
  `country` varchar(150) COLLATE utf8_unicode_ci DEFAULT '',
  `postcode` varchar(150) COLLATE utf8_unicode_ci DEFAULT '',
  `phone` varchar(150) COLLATE utf8_unicode_ci DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `account_customer_address_customer_id_index` (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `account_password_reminders`
--

CREATE TABLE IF NOT EXISTS `account_password_reminders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(150) COLLATE utf8_unicode_ci DEFAULT '',
  `token` varchar(150) COLLATE utf8_unicode_ci DEFAULT '',
  `created_at` varchar(150) COLLATE utf8_unicode_ci DEFAULT '',
  `store_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `account_password_reminders_store_id_index` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE IF NOT EXISTS `cart` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `status` varchar(150) COLLATE utf8_unicode_ci DEFAULT 'open',
  `store_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `cart_customer_id_index` (`customer_id`),
  KEY `cart_store_id_index` (`store_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `customer_id`, `status`, `store_id`, `created_at`, `updated_at`) VALUES
(1, 1, 'open', 1, '2015-05-06 09:11:17', '2015-05-06 09:15:34');

-- --------------------------------------------------------

--
-- Table structure for table `cart_item`
--

CREATE TABLE IF NOT EXISTS `cart_item` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cart_id` int(10) unsigned DEFAULT NULL,
  `product_id` int(10) unsigned DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `total` decimal(12,4) DEFAULT NULL,
  `store_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cart_item_cart_id_index` (`cart_id`),
  KEY `cart_item_product_id_index` (`product_id`),
  KEY `cart_item_store_id_index` (`store_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `cart_item`
--

INSERT INTO `cart_item` (`id`, `cart_id`, `product_id`, `qty`, `total`, `store_id`) VALUES
(1, 1, 2, 1, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cart_payment`
--

CREATE TABLE IF NOT EXISTS `cart_payment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cart_id` int(10) unsigned DEFAULT NULL,
  `customer_address_id` int(10) unsigned DEFAULT NULL,
  `method` varchar(150) COLLATE utf8_unicode_ci DEFAULT '',
  `number` int(11) DEFAULT NULL,
  `total` decimal(12,4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cart_payment_cart_id_index` (`cart_id`),
  KEY `cart_payment_customer_address_id_index` (`customer_address_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cart_shipment`
--

CREATE TABLE IF NOT EXISTS `cart_shipment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cart_id` int(10) unsigned DEFAULT NULL,
  `customer_address_id` int(10) unsigned DEFAULT NULL,
  `method` varchar(150) COLLATE utf8_unicode_ci DEFAULT '',
  `number` int(11) DEFAULT NULL,
  `total` decimal(12,4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cart_shipment_cart_id_index` (`cart_id`),
  KEY `cart_shipment_customer_address_id_index` (`customer_address_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `cart_shipment`
--

INSERT INTO `cart_shipment` (`id`, `cart_id`, `customer_address_id`, `method`, `number`, `total`) VALUES
(1, 1, NULL, NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `catalog_category`
--

CREATE TABLE IF NOT EXISTS `catalog_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci DEFAULT '',
  `description` longtext COLLATE utf8_unicode_ci,
  `url` varchar(150) COLLATE utf8_unicode_ci DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `catalog_category_category_id_index` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `catalog_category`
--

INSERT INTO `catalog_category` (`id`, `category_id`, `name`, `description`, `url`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Root Category', NULL, '554a038e3b7a9', '2015-05-06 09:05:34', '2015-05-06 09:05:34'),
(2, 1, 'Child Category 1', NULL, '554a03de2d01d', '2015-05-06 09:06:54', '2015-05-06 09:06:54'),
(3, 1, 'Child Category 2', NULL, '554a03de41462', '2015-05-06 09:06:54', '2015-05-06 09:06:54'),
(4, 3, 'Grandchild Category 1', NULL, '554a03de539d1', '2015-05-06 09:06:54', '2015-05-06 09:06:54'),
(5, 3, 'Grandchild Category 2', NULL, '554a03de61de8', '2015-05-06 09:06:54', '2015-05-06 09:06:54');

-- --------------------------------------------------------

--
-- Table structure for table `catalog_category_product`
--

CREATE TABLE IF NOT EXISTS `catalog_category_product` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned DEFAULT NULL,
  `category_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `catalog_category_product_product_id_index` (`product_id`),
  KEY `catalog_category_product_category_id_index` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=17 ;

--
-- Dumping data for table `catalog_category_product`
--

INSERT INTO `catalog_category_product` (`id`, `product_id`, `category_id`) VALUES
(1, 1, 3),
(2, 1, 5),
(3, 2, 2),
(4, 2, 5),
(5, 3, 3),
(6, 3, 5),
(7, 4, 3),
(8, 4, 5),
(9, 5, 3),
(10, 5, 5),
(11, 6, 3),
(12, 6, 5),
(13, 7, 3),
(14, 7, 5),
(15, 8, 3),
(16, 8, 5);

-- --------------------------------------------------------

--
-- Table structure for table `catalog_product`
--

CREATE TABLE IF NOT EXISTS `catalog_product` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sku` varchar(150) COLLATE utf8_unicode_ci DEFAULT '',
  `name` varchar(150) COLLATE utf8_unicode_ci DEFAULT '',
  `price` decimal(12,4) DEFAULT NULL,
  `url` varchar(150) COLLATE utf8_unicode_ci DEFAULT '',
  `special_price` decimal(12,4) DEFAULT NULL,
  `store_id` int(10) unsigned DEFAULT NULL,
  `disease` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `catalog_product_store_id_index` (`store_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `catalog_product`
--

INSERT INTO `catalog_product` (`id`, `sku`, `name`, `price`, `url`, `special_price`, `store_id`, `disease`, `created_at`, `updated_at`) VALUES
(1, 'test-1', 'Test Product 1', 8.9900, '554a03de710b0', 0.0000, 1, 'Diabetes', '2015-05-06 09:06:54', '2015-05-06 09:06:55'),
(2, 'test-2', 'Test Product 2', 9.9900, '554a03de7e4fc', 5.9900, 1, 'flu', '2015-05-06 09:06:54', '2015-05-06 09:06:55'),
(3, 'test-3', 'Test Product 3', 8.9900, '554a03de9b80e', 0.0000, 1, 'flu,typhoid', '2015-05-06 09:06:54', '2015-05-06 09:06:55'),
(4, 'test-4', 'Test Product 4', 8.9900, '554a03dea6ed0', 0.0000, 1, '', '2015-05-06 09:06:54', '2015-05-06 09:06:55'),
(5, 'test-5', 'Test Product 5', 9.9900, '554a03deb3eed', 5.9900, 1, '', '2015-05-06 09:06:54', '2015-05-06 09:06:55'),
(6, 'test-6', 'Test Product 6', 8.9900, '554a03debf5a8', 0.0000, 1, '', '2015-05-06 09:06:54', '2015-05-06 09:06:56'),
(7, 'test-7', 'Test Product 7', 8.9900, '554a03decc5bb', 0.0000, 1, '', '2015-05-06 09:06:54', '2015-05-06 09:06:56'),
(8, 'test-8', 'Test Product 8', 9.9900, '554a03ded7ce8', 5.9900, 1, 'Blood pressure', '2015-05-06 09:06:54', '2015-05-06 18:22:37'),
(9, 'test ', 'test ', 15.0000, '554a863a15e96', 0.0000, 1, 'test', '2015-05-06 18:23:06', '2015-05-06 18:23:06');

-- --------------------------------------------------------

--
-- Table structure for table `diet`
--

CREATE TABLE IF NOT EXISTS `diet` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `disease_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `diet`
--

INSERT INTO `diet` (`id`, `image`, `disease_id`, `created_at`, `updated_at`) VALUES
(3, '1.jpg', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, '2.jpg', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, '3.png', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, '4.png', 4, '0000-00-00 00:00:00', '2015-05-06 19:49:50');

-- --------------------------------------------------------

--
-- Table structure for table `disease`
--

CREATE TABLE IF NOT EXISTS `disease` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `disease_name_unique` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `disease`
--

INSERT INTO `disease` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Diabetes', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Blood pressure', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'flu', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'typhoid', '0000-00-00 00:00:00', '2015-05-06 19:45:10');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2015_05_06_120422_migration_1430913862', 1),
('2015_05_06_120422_migration_disease', 2),
('2015_05_06_120422_migration_diet', 3);

-- --------------------------------------------------------

--
-- Table structure for table `store`
--

CREATE TABLE IF NOT EXISTS `store` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned DEFAULT NULL,
  `theme_id` int(10) unsigned DEFAULT NULL,
  `default` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `store_category_id_index` (`category_id`),
  KEY `store_theme_id_index` (`theme_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `store`
--

INSERT INTO `store` (`id`, `category_id`, `theme_id`, `default`) VALUES
(1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `store_config`
--

CREATE TABLE IF NOT EXISTS `store_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `store_id` int(10) unsigned DEFAULT NULL,
  `key` varchar(150) COLLATE utf8_unicode_ci DEFAULT '',
  `value` varchar(150) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `store_config_store_id_index` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `theme`
--

CREATE TABLE IF NOT EXISTS `theme` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `theme_id` int(10) unsigned DEFAULT NULL,
  `code` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci DEFAULT '',
  `store_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `theme_code_unique` (`code`),
  KEY `theme_theme_id_index` (`theme_id`),
  KEY `theme_store_id_index` (`store_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `theme`
--

INSERT INTO `theme` (`id`, `theme_id`, `code`, `name`, `store_id`) VALUES
(1, NULL, 'default', 'Default Theme', 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `account_customer`
--
ALTER TABLE `account_customer`
  ADD CONSTRAINT `account_customer_customer_address_id_foreign` FOREIGN KEY (`customer_address_id`) REFERENCES `account_customer_address` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `account_customer_store_id_foreign` FOREIGN KEY (`store_id`) REFERENCES `store` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `account_customer_address`
--
ALTER TABLE `account_customer_address`
  ADD CONSTRAINT `account_customer_address_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `account_customer` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `account_password_reminders`
--
ALTER TABLE `account_password_reminders`
  ADD CONSTRAINT `account_password_reminders_store_id_foreign` FOREIGN KEY (`store_id`) REFERENCES `store` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cart`
--
ALTER TABLE `cart`
  ADD CONSTRAINT `cart_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `account_customer` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cart_store_id_foreign` FOREIGN KEY (`store_id`) REFERENCES `store` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cart_item`
--
ALTER TABLE `cart_item`
  ADD CONSTRAINT `cart_item_cart_id_foreign` FOREIGN KEY (`cart_id`) REFERENCES `cart` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cart_item_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `catalog_product` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cart_item_store_id_foreign` FOREIGN KEY (`store_id`) REFERENCES `store` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cart_payment`
--
ALTER TABLE `cart_payment`
  ADD CONSTRAINT `cart_payment_cart_id_foreign` FOREIGN KEY (`cart_id`) REFERENCES `cart` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cart_payment_customer_address_id_foreign` FOREIGN KEY (`customer_address_id`) REFERENCES `account_customer_address` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cart_shipment`
--
ALTER TABLE `cart_shipment`
  ADD CONSTRAINT `cart_shipment_cart_id_foreign` FOREIGN KEY (`cart_id`) REFERENCES `cart` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cart_shipment_customer_address_id_foreign` FOREIGN KEY (`customer_address_id`) REFERENCES `account_customer_address` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `catalog_category`
--
ALTER TABLE `catalog_category`
  ADD CONSTRAINT `catalog_category_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `catalog_category` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `catalog_category_product`
--
ALTER TABLE `catalog_category_product`
  ADD CONSTRAINT `catalog_category_product_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `catalog_category` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `catalog_category_product_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `catalog_product` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `catalog_product`
--
ALTER TABLE `catalog_product`
  ADD CONSTRAINT `catalog_product_store_id_foreign` FOREIGN KEY (`store_id`) REFERENCES `store` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `store`
--
ALTER TABLE `store`
  ADD CONSTRAINT `store_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `catalog_category` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `store_theme_id_foreign` FOREIGN KEY (`theme_id`) REFERENCES `theme` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `store_config`
--
ALTER TABLE `store_config`
  ADD CONSTRAINT `store_config_store_id_foreign` FOREIGN KEY (`store_id`) REFERENCES `store` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `theme`
--
ALTER TABLE `theme`
  ADD CONSTRAINT `theme_store_id_foreign` FOREIGN KEY (`store_id`) REFERENCES `store` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `theme_theme_id_foreign` FOREIGN KEY (`theme_id`) REFERENCES `theme` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
