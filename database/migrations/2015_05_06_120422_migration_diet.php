<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MigrationDiet extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		
        Schema::create("diet", function (Blueprint $table){

            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->string("image", 250);
            $table->integer('disease_id');
            $table->timestamps();

        });


	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{		
  
        Schema::drop("diet");
	}

}
