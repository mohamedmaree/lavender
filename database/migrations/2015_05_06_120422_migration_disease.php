<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MigrationDisease extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		
        Schema::create("disease", function (Blueprint $table){

            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->string("name", 250)->unique();
            $table->timestamps();

        });


	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{		
  
        Schema::drop("disease");
	}

}
