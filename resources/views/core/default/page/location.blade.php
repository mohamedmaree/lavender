@extends('layouts.single')

@section('head.title')
Home
@stop

@section('content')
    <style>
      #holder {
        height: 300px;
        margin: 0px;
        padding: 0px
      }
    </style>

    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>
    <script>
function distance(lon1, lat1, lon2, lat2) {
  var R = 6371; // Radius of the earth in km
  var dLat = (lat2-lat1).toRad();  // Javascript functions in radians
  var dLon = (lon2-lon1).toRad(); 
  var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
          Math.cos(lat1.toRad()) * Math.cos(lat2.toRad()) * 
          Math.sin(dLon/2) * Math.sin(dLon/2); 
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
  var d = R * c; // Distance in km
  return d;
}
/** Converts numeric degrees to radians */
if (typeof(Number.prototype.toRad) === "undefined") {
  Number.prototype.toRad = function() {
    return this * Math.PI / 180;
  }
}

function initialize() {

window.navigator.geolocation.getCurrentPosition(function(pos) {
  console.log(pos); 
  var f_distance= distance(pos.coords.longitude, pos.coords.latitude, 30.786508599999994, 30.786508599999994)  ;
  var s_distance= distance(pos.coords.longitude, pos.coords.latitude, 31.000375699999996, 31.000375699999996)  ;
  if(f_distance > s_distance){
            // alert('f > s '+ f_distance+'-------'+s_distance);
            //استخدم احداثيات التانى
            alert('that is the closest department for you ');
            var latlon = new google.maps.LatLng(pos.coords.latitude,pos.coords.longitude);
  }else{
            // alert('s > f '+ s_distance+'-------'+f_distance);
            //استخدم احداثيات الاول
            alert('that is the closest department for you ');
            var latlon = new google.maps.LatLng(pos.coords.latitude,pos.coords.longitude);
  }
      
  var mapOptions = {
    zoom: 10,
    center: latlon
  }
  var map = new google.maps.Map(document.getElementById('holder'), mapOptions);

  var marker = new google.maps.Marker({
      position: latlon,
      map: map,
      title: 'اسم الفرع '
  });

});

}

google.maps.event.addDomListener(window, 'load', initialize);

    </script>
    <div id="holder">here</div>

@stop