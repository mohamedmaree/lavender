<?php
namespace App\Database;

use Lavender\Database\Entity;

class Diet extends Entity
{

    protected $entity = 'diet';

    protected $table = 'diet';

    public $timestamps = true;

}