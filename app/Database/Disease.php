<?php
namespace App\Database;

use Lavender\Database\Entity;

class Disease extends Entity
{

    protected $entity = 'disease';

    protected $table = 'disease';

    public $timestamps = true;

}