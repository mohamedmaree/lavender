<?php
namespace App\Form\Customer;

use Lavender\Support\Form;

class Diet extends Form
{

    public function __construct($params)
    {
        $this->options['action'] = url('diets/');

                $this->addField('disease', [
                    'label' => 'Are you have any of these Diseases ?',
                    'type'  => 'radiolist',
                    'resource' => 'disease_diets',
                ]);
        

        $this->addField('submit', [
            'type' => 'button',
            'value' => 'get Diet for that diseases ',
            'options' => ['type' => 'submit'],
        ]);
    }

}