<?php
namespace App\Form\Customer;

use Lavender\Support\Form;

class Disease extends Form
{

    public function __construct($params)
    {
        $this->options['action'] = url('diseases/');

                $this->addField('disease[]', [
                    'label' => 'Are you have any of these Diseases ?',
                    'type'  => 'checkboxlist',
                    'resource' => 'disease_diseases',
                ]);
        

        $this->addField('submit', [
            'type' => 'button',
            'value' => 'get products for that diseases ',
            'options' => ['type' => 'submit'],
        ]);
    }

}