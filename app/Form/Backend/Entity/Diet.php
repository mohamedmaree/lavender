<?php
namespace App\Form\Backend\Entity;

use App\Form\Backend\Entity;

class Diet extends Entity
{

    public $diet;

    public function __construct($params)
    {
        $this->diet = $params->entity;

        $this->addField('image', [
            'label' => 'Image',
            'type' => 'text',
            'validate' => ['required'],
            'value' => $this->diet->image,
        ]);

        $this->addField('disease_id', [
            'label' => 'disease_id',
            'type' => 'text',
            'value' => '',
        ]);
        parent::__construct($params);
    }

}