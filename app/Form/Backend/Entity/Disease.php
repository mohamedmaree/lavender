<?php
namespace App\Form\Backend\Entity;

use App\Form\Backend\Entity;

class Disease extends Entity
{

    public $disease;

    public function __construct($params)
    {
        $this->disease = $params->entity;

        $this->addField('name', [
            'label' => 'Name',
            'type' => 'text',
            'validate' => ['required'],
            'value' => $this->disease->name,
        ]);

        parent::__construct($params);
    }

}