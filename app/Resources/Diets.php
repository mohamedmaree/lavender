<?php
namespace App\Resources;

use Illuminate\Contracts\Support\Arrayable;
use Lavender\Contracts\Entity;
use App\Disease;

class Diets  implements Arrayable
{
    // protected $cart;

    // public function __construct(Cart $cart)
    // {
    //     $this->cart = $cart;
    // }

    // public function getShipments()
    // {
    //     return $this->cart->getShipments();
    // }

    // public function getMethods(Entity $address)
    // {
    //     return event(new CollectMethods($address));
    // }

    public function toArray()
    {
        $results = [];
        $disease=new Disease();
            foreach($disease->getAll() as $disease){

                $results[] = [
                    'label'   => $disease->name,
                    'name'    => 'disease_id',
                    'value'   => $disease->id,
                ];

            }

        return $results;
    }

}