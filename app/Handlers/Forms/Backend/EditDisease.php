<?php
namespace App\Handlers\Forms\Backend;

use App\Support\Facades\Message;
use Lavender\Contracts\Form;

class EditDisease
{

    /**
     * @param $data
     */
    public function handle_disease(Form $form)
    {
        $request = $form->request->all();

        $disease = $form->disease;

        $new = !$disease->exists;

        $disease->fill($request);

        $disease->save();

        Message::addSuccess(sprintf(
            "disease \"%s\" was %s.",
            $disease->name,
            $new ? 'created' : 'updated'
        ));
    }

    /**
     * @param $data
     */
    // public function handle_categories(Form $form)
    // {
    //     $request = $form->request->all();

    //     $disease = $form->disease;

    //     //todo fix detach / update (doesn't work sequentially without cloning entity)
    //     $cloned = clone $disease;
    //     $cloned->categories()->detach();

    //     $disease->update(['categories' => ['category' => $request['category']]]);

    //     Message::addSuccess(sprintf(
    //         "disease \"%s\" was updated.",
    //         $disease->name
    //     ));
    // }

}