<?php
namespace App\Handlers\Forms\Backend;

use App\Support\Facades\Message;
use Lavender\Contracts\Form;

class EditDiet
{

    /**
     * @param $data
     */
    public function handle_diet(Form $form)
    {
        $request = $form->request->all();

        $diet = $form->diet;

        $new = !$diet->exists;

        $diet->fill($request);

        $diet->save();

        Message::addSuccess(sprintf(
            "diet \"%s\" was %s.",
            $diet->id,
            $new ? 'created' : 'updated'
        ));
    }

    /**
     * @param $data
     */
    // public function handle_categories(Form $form)
    // {
    //     $request = $form->request->all();

    //     $diet = $form->diet;

    //     //todo fix detach / update (doesn't work sequentially without cloning entity)
    //     $cloned = clone $diet;
    //     $cloned->categories()->detach();

    //     $diet->update(['categories' => ['category' => $request['category']]]);

    //     Message::addSuccess(sprintf(
    //         "diet \"%s\" was updated.",
    //         $diet->name
    //     ));
    // }

}