<?php
namespace App\Http\Controllers\Backend\Catalog;

use App\Http\Controller\BackendEntity;
use Lavender\Contracts\Entity;
use Lavender\Http\FormRequest;

class DietController extends BackendEntity
{
	public function getEdit($id)
    {
        if($model = $this->validateEntity('diet', $id)){

            $this->loadLayout();

            $tabs[] = [
                'label' => "General",
                'content' => form('edit_diet', ['entity' => $model])
            ];

            // if($model->exists){

            //     $tabs[] = [
            //         'label'   => "Categories",
            //         'content' => form('edit_product_categories', ['entity' => $model])
            //     ];

            // }
            return view('backend.tabs')
                ->with('title', $model->getEntityName())
                ->with('tabs', $tabs);
        }

        return redirect('backend');
    }

	public function getIndex()
	{
        if($model = $this->validateEntity('diet')){

            $columns =  [
                'Id'           => 'id',
                'diet image'    => 'image',
                'disease Id'   => 'disease_id',
                'Last Updated' => 'updated_at'
            ];

            $this->loadLayout();

            $new_button = url('backend/diet/edit/new');

            compose_section(
                'backend.grid',
                'new_button',
                "<button onclick=\"window.location='{$new_button}';\">Add new diet</button>"
            );

            compose_section(
                'backend.grid',
                'mass_actions',
                "<select><option>Action</option></select>"
            );

            return view('backend.grid')
                ->with('title',    'diet')
                ->with('edit_url', 'backend/diet/edit')
                ->with('rows',      $model->all($columns))
                ->with('headers',   $this->tableHeaders($model, $columns));
        }

        return redirect('backend');
	}


    /**
     * Update a product model
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postEdit(FormRequest $request, $id)
    {
        if($model = $this->validateEntity('diet', $id)){

            $new = !$model->exists;

            form('edit_diet', ['entity' => $model])->handle($request);

            if($new && $model->exists) return redirect()->to('backend/diet/edit/'.$model->id);

        }

        return redirect()->back();
    }


    /**
     * Update product categories
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function postCategories(FormRequest $request, $id)
    // {
    //     if($model = $this->validateEntity('diet', $id)){

    //         form('edit_product_categories', ['entity' => $model])->handle($request);

    //     }

    //     return redirect()->back();
    // }

}