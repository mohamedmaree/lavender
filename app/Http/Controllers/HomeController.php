<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controller\Frontend;

class HomeController extends Frontend
{
	public function __construct()
	{
        $this->loadLayout();
	}


	public function index()
	{
		return view('page.home');
	}

	public function getLocation()
	{
		return view('page.location');
	}

}
