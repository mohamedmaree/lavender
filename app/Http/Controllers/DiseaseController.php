<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controller\Frontend;
use App\Disease;

class DiseaseController extends Frontend
{
	public function __construct()
	{
        $this->loadLayout();
	}


	public function index(Disease $disease)
	{
		return view('disease.index')->with('diseases',$this->diseases);
	}

	public function userDiseases(Request $request)
	{
		\Session::forget('userdisease');
		$userdiseases=array();
		if(count($request->input('disease')) > 0 ){
           foreach($request->input('disease') as $disease){
           	  $userdiseases[]=$disease;
           }
           \Session::put('userdisease', $userdiseases);
		}
     return redirect('catalog/category/554a03de41462');
	}

}
