<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controller\Frontend;
use App\Diet;

class DietController extends Frontend
{
	public function __construct()
	{
        $this->loadLayout();
	}

	public function index()
	{
		return view('diet.index');
	}


	public function diets(Request $request)
	{
		if($request->has('disease_id')){
            $diet = entity('diet')->findByAttribute('disease_id', $request->input('disease_id'));
           
            return view('diet.show')
                ->withDiet($diet);
        }else{
            return redirect()->back();
        }
        
        throw new HttpException(404, 'Category not found.');
	}

}
